def main_feature():
    some_helper_function()
    print("This is the main feature")


def some_helper_function():
    print("This is a helper function")


    new_feature()
    another_new_feature()
    print("This is a helper function")


def new_feature():
    print("This is a new feature")


def another_new_feature():
    print("This is another new feature")


if __name__ == "__main__":
    main_feature()